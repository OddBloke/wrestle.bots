import base64
import logging
import os
import os.path

import requests

from polybot import Bot
from profanityfilter import ProfanityFilter


LOG = logging.getLogger()

# Some words aren't in the standard censor list, but should be (or are in a
# corpus with weird formatting/spelling). b64encode'd so that we don't have to
# read them incidentally
BLACKLISTED_WORDS = [b"bmFwcHk=", b"Y3JpcC1wbGU=", b"dHJhbnN2ZXN0aXRl"]

# Where to cache responses from Cagematch
CACHE_DIR = "cachematch"

# Censoring some words makes wrestling promos look like ***
WHITELISTED_WORDS = [
    "XXX",
    "arse",
    "ass",
    "bastards",
    "boobs",
    "crap",
    "damn",
    "gay",
    "hell",
    "penis",
    "piss",
    "porn",
    "queer",
    "screw",
    "sex",
    "sexy",
    "turd",
]

# Some wrestlers are a bummer, let's not put their names in people's feeds
WRESTLER_BLACKLIST = [
    "Alberto del Rio",
    "Bobbitt",
    "Chris Benoit",
    "Colin Cassady",
    "Del Rio",
    "Enzo Amore",
    "Hollywood Hogan",
    "Hulk Hogan",
    "Michael Elgin",
    "Ron Harris",
    "Sexy Star",
    "The Warrior",
    "Ultimate Warrior",
]


def get_profanity_filter():
    profanity_filter = ProfanityFilter()
    for word in WHITELISTED_WORDS:
        word = word.lower()
        LOG.info("Uncensoring %s", word)
        profanity_filter.remove_word(word)
    LOG.info("Censoring: %s", BLACKLISTED_WORDS)
    profanity_filter.append_words(
        [
            base64.b64decode(b64_word).decode("utf-8")
            for b64_word in BLACKLISTED_WORDS
        ]
    )
    return profanity_filter


def requests_get(url):
    LOG.info("Fetching URL: %s", url)
    response = requests.get(url)
    if response.ok:
        if url.startswith("https://cagematch.net/"):
            url_path = url[len("https://cagematch.net/") :]
            cache_path = os.path.join(CACHE_DIR, url_path)
            LOG.info("Cache path: %s", cache_path)
            with open(cache_path, "wb") as f:
                f.write(response.content)
    return response


class WrestleBot(Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.profanity_filter = get_profanity_filter()

        if not os.path.isdir(CACHE_DIR):
            LOG.info("Creating cache directory: %s", CACHE_DIR)
            os.mkdir(CACHE_DIR)

    def post(self, content, *args, **kwargs):
        censored_content = self.profanity_filter.censor(content)
        if content != censored_content:
            LOG.info("CENSORED CONTENT:\n%s", content)
        super().post(censored_content)
