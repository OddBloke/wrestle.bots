#!/usr/bin/env python3
import datetime
import logging
import random
import time
from collections import namedtuple

import pytz
from bs4 import BeautifulSoup
from polybot import Bot

from lib import WRESTLER_BLACKLIST, requests_get


LOG = logging.getLogger()


# Cagematch has records back to 1887, so look in years with that as a lower
# bound
EARLIEST_YEAR = 1887

# Use PST so that we don't switch to a new day until all of North America is in
# that day
UTC_NOW = pytz.utc.localize(datetime.datetime.utcnow())
PST_NOW = UTC_NOW.astimezone(pytz.timezone("America/Los_Angeles"))
TODAY = PST_NOW.date()
LAST_YEAR = TODAY.year - 1

# The URL which lists all the events for a particular date
EVENTS_ON_DAY_URL_TEMPLATE = (
    "https://cagematch.net/?id=1&view=results"
    "&Day={day:02}&Month={month:02}&Year={year}"
)

POST_TEMPLATE = """\
{long_date}: {event.promotion} at {event.venue} in {event.location}

{event.match.result} ({event.match.stipulation})

{event.url}

#wrestling
"""

Match = namedtuple("Match", ("stipulation", "result"))


class Event:
    def __init__(self, title, url):
        self.title = title
        self.url = url
        self._soup = None
        self._info_box = None
        self._match = None

    def __str__(self):
        return "{} <{}>".format(self.title, self.url)

    @property
    def soup(self):
        if self._soup is None:
            response = _requests_get(self.url)
            response.raise_for_status()
            self._soup = BeautifulSoup(response.text, "html.parser")
        return self._soup

    @property
    def info_box(self):
        if self._info_box is None:
            rows = self.soup.select(
                "div.InformationBoxTable .InformationBoxRow"
            )
            self._info_box = {}
            for row in rows:
                key = row.find(
                    attrs={"class": "InformationBoxTitle"}
                ).text.strip()
                contents = row.find(attrs={"class": "InformationBoxContents"})
                values = contents.find_all("a")
                if not values:
                    # No links, use the whole text
                    value = contents.text.strip()
                else:
                    # Promotion: at least sometimes has multiple values, let's
                    # just take the first one for now
                    value = values[0].text.strip()
                self._info_box[key] = value
        return self._info_box

    @property
    def promotion(self):
        return self.info_box.get("Promotion:", "Unknown Promotion")

    @property
    def venue(self):
        return self.info_box.get("Arena:", "Unknown Arena")

    @property
    def location(self):
        return self.info_box.get("Location:", "Unknown Location")

    @property
    def match(self):
        if self._match is None:
            candidates = self.matches.copy()
            random.shuffle(candidates)
            for candidate in candidates:
                if any(
                    [
                        blacklisted in candidate.result
                        for blacklisted in WRESTLER_BLACKLIST
                    ]
                ):
                    LOG.info(
                        "Found blacklisted wrestler in %s; skipping.",
                        candidate,
                    )
                    continue
                self._match = candidate
                break
        return self._match

    @property
    def matches(self):
        match_elements = self.soup.select(".Match")
        matches = []
        for match_element in match_elements:
            stipulation = match_element.select(".MatchType")[0].text.strip()
            result = match_element.select(".MatchResults")[0].text.strip()
            matches.append(Match(stipulation, result))
        return matches


class ThisDayInHistoryBot(Bot):
    def get_today_page_with_results(self):
        known_skips = self.state["known_skips"][TODAY]
        years = list(range(EARLIEST_YEAR, LAST_YEAR))
        random.shuffle(years)
        while len(years):
            year = years.pop(0)
            LOG.info("Selected year: %s", year)
            if year in known_skips:
                LOG.info("Year is a known_skip; next!")
                continue

            url = EVENTS_ON_DAY_URL_TEMPLATE.format(
                day=TODAY.day, month=TODAY.month, year=year
            )
            LOG.info("Determined URL: %s", url)
            response = requests_get(url)

            # Skip this for today once we've processed it once, whether or not
            # there are events on the day
            known_skips.append(year)
            if (
                "No items were found that match the search parameters."
                in response.text
            ):
                LOG.info("No events found, trying another year after a pause")
                time.sleep(5)
                continue

            return response.text, datetime.date(year, TODAY.month, TODAY.day)
        raise Exception("No remaining years to display!")

    def parse_events_out_of_day_page(self, page):
        soup = BeautifulSoup(page, "html.parser")
        rows = soup.select("table.TBase tr:not(.THeaderRow)")
        events = []
        for row in rows:
            links = row.find_all("a")
            for link in links:
                url = link.get("href", "")
                if "id=1" in url and "page=2" not in url:
                    # We want a link to an event page, but not the card tab
                    LOG.info("Found event: %s", link.text)
                    events.append(
                        Event(
                            link.text, "https://cagematch.net/{}".format(url)
                        )
                    )
        return events

    def init(self):
        LOG.info("Running for: %s", TODAY)

        if "known_skips" not in self.state:
            self.state["known_skips"] = {}
        if TODAY not in self.state["known_skips"]:
            self.state["known_skips"][TODAY] = []
        known_skips = self.state["known_skips"][TODAY]
        LOG.info("Known skips (%d): %s", len(known_skips), sorted(known_skips))

        if "incidental_collection" not in self.state:
            self.state["incidental_collection"] = {}

        for key in ["promotion", "venue", "location", "result", "stipulation"]:
            if key not in self.state["incidental_collection"]:
                self.state["incidental_collection"][key] = set()

    def incidental_collect(self, event):
        LOG.info("Performing incidental collection for event: %s", event)
        for key in ["promotion", "venue", "location"]:
            value = getattr(event, key, None)
            if value and not value.startswith("Unknown "):
                self.state["incidental_collection"][key].add(value)
        for match in event.matches:
            self.state["incidental_collection"]["result"].add(match.result)
            self.state["incidental_collection"]["stipulation"].add(
                match.stipulation
            )

    def incidental_dump(self):
        LOG.info("Dumping incidental collection state...")
        for key, value in self.state["incidental_collection"].items():
            with open("incidental-{}.txt".format(key), "w") as f:
                f.write("\n".join(sorted(value)) + "\n")

    def main(self):
        self.init()
        page, selected_date = self.get_today_page_with_results()
        events = self.parse_events_out_of_day_page(page)
        random.shuffle(events)
        for event in events:
            self.incidental_collect(event)
            if event.match is None:
                continue
            post_text = POST_TEMPLATE.format(
                long_date=str(selected_date), event=event
            )
            break
        else:
            raise Exception("No event found with non-blacklisted result")
        self.post(post_text)
        self.incidental_dump()


if __name__ == "__main__":
    ThisDayInHistoryBot("ThisDayInHistory").run()
