#!/usr/bin/env python3
import scrapy
from scrapy.crawler import CrawlerProcess


class CagematchPromoSpider(scrapy.Spider):
    name = "cagematch-promos"
    allowed_domains = ["cagematch.net"]
    start_urls = ["https://www.cagematch.net/?id=93&view=list"]

    def parse(self, response):
        promo_links = response.css("table.TBase tr td:nth-child(4) a")
        for promo_link in promo_links:
            link_target = promo_link.css("a::attr(href)").extract_first()
            if "id=93" not in link_target:
                self.logger.warning("Skipping non-promo link: %s", link_target)
                continue
            yield response.follow(link_target, callback=self.parse_promo_page)
        # Flip through pages
        next_link = response.xpath(
            '//a[div[text()=">"]]/@href'
        ).extract_first()
        if next_link:
            yield response.follow(next_link, callback=self.parse)

    def parse_promo_page(self, response):
        result = {"_type": "promo", "url": response.url}

        # Extract metadata from the Promo Data section at the top of the page
        info_box_dict = {
            row.css(".InformationBoxTitle::text").get(): row.css(
                ".InformationBoxContents"
            )[0]
            for row in response.css(".InformationBoxRow")
        }
        if "Title:" in info_box_dict:
            result["title"] = info_box_dict["Title:"].css("::text").get()
        if "Date:" in info_box_dict:
            result["date"] = info_box_dict["Date:"].css("::text").get()
        if "Promotion:" in info_box_dict:
            promotion_link = info_box_dict["Promotion:"]
            result["promotion"] = {
                "_type": "promotion",
                "name": promotion_link.css("::text").get(),
                "url": response.urljoin(
                    promotion_link.css("::attr(href)").get()
                ),
            }
        if "Event:" in info_box_dict:
            event_link = info_box_dict["Event:"].css("a")[0]
            result["event"] = {
                "_type": "event",
                "name": event_link.css("::text").get(),
                "url": response.urljoin(event_link.css("::attr(href)").get()),
            }
        if "Worker(s):" in info_box_dict:
            result["wrestlers"] = []
            for wrestler_link in info_box_dict["Worker(s):"].css("a"):
                result["wrestlers"].append(
                    {
                        "_type": "wrestler",
                        "name": wrestler_link.css("::text").get(),
                        "url": response.urljoin(
                            wrestler_link.css("::attr(href)").get()
                        ),
                    }
                )

        # Now iterate over the parts of the promo building up a sequence of
        # events
        promo_script = []
        promo_element = response.css(".Text")[0]
        promo_nodes = promo_element.xpath("./child::node()")
        current_actor = None
        current_promo_text = ""

        def store_line():
            nonlocal current_promo_text
            if current_promo_text.startswith(': "'):
                current_promo_text = current_promo_text[3:]
            current_promo_text = current_promo_text.strip().strip('"')
            if current_actor is not None and current_promo_text:
                promo_script.append(
                    {
                        "_type": "line",
                        "actor": current_actor,
                        "text": current_promo_text,
                    }
                )
            current_promo_text = ""

        for node in promo_nodes:
            if isinstance(node.root, str):
                node_text = node.root
            else:
                node_text = node.css("::text").get()
            if not node_text:
                # No text in this node, nothing to add
                continue

            node_cls = node.xpath("@class").get()
            if node_cls == "TextHighlightBold":
                # TextHighlightBold spans contain the name of an actor; this
                # signals a change of actor, so store the last script element
                # before moving on
                store_line()
                current_actor = node_text.strip().strip(":")
                continue
            elif node_cls == "TextLowlight":
                # TextLowlight spans are used for stage directions (usually in
                # German).  Unlike the text of the promos, the span _contains_
                # the stage direction text.
                store_line()
                promo_script.append(
                    {"_type": "stage_direction", "text": node_text}
                )
                continue
            elif node_cls:
                self.logger.warning(
                    "Unexpected promo node class: %s (in %s on %s)",
                    node_cls,
                    node,
                    response.url,
                )
                continue

            # We only get here if we're dealing with a node with text but
            # without a class; this should be a piece of promo text.
            current_promo_text += node_text
        # If the promo ends on a line, ensure we store it
        store_line()

        result["script"] = promo_script
        yield result


if __name__ == "__main__":
    process = CrawlerProcess(
        settings={
            "FEED_FORMAT": "json",
            "FEED_URI": "promos.json",
            "JOBDIR": "crawls/promos-1",
        }
    )
    process.crawl(CagematchPromoSpider)
    process.start()
