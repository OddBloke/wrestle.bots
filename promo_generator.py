#!/usr/bin/env python3
import json
import logging
import random

import markovify

from lib import WRESTLER_BLACKLIST, WrestleBot


LOG = logging.getLogger()


def combine_promos():
    with open("data/promos.json") as f:
        promos = json.load(f)
    full_corpus = ""
    wrestlers = set()
    for promo in promos:
        for script_part in promo["script"]:
            if script_part["_type"] != "line":
                continue
            full_corpus += " "
            full_corpus += script_part["text"]
            wrestlers.add(script_part["actor"])
    return full_corpus, list(wrestlers)


class WrestlingPromosBot(WrestleBot):
    def main(self):
        corpus, wrestler_names = combine_promos()
        text_model = markovify.Text(corpus)
        promo = ""
        wrestlers = []
        while True:
            next_sentence = None
            LOG.info("Current wrestlers: %s", wrestlers)
            if wrestlers:
                last_wrestler_words = wrestlers[-1].split()
                prefix_lengths = [1]
                if len(last_wrestler_words) > 1 and random.random() > 0.5:
                    # Only use two words half the time, to avoid generating the
                    # same sentence over and over again for names not in the
                    # corpus much
                    prefix_lengths.insert(0, 2)
                LOG.info("Using prefix lengths: %s", prefix_lengths)
                for prefix_length in prefix_lengths:
                    prefix = " ".join(
                        last_wrestler_words[0:prefix_length]
                    ).title()
                    LOG.info(
                        "Attempting to make sentence with prefix: %s", prefix
                    )
                    try:
                        next_sentence = text_model.make_sentence_with_start(
                            prefix
                        )
                    except KeyError:
                        # This means we couldn't create a sentence with that
                        # prefix; try any more prefix lengths or fall through
                        # to regular sentence generation
                        LOG.info("Prefix attempt failed.")
                    else:
                        # We have a sentence, nothing more to do
                        break
            if not (next_sentence):
                LOG.info("Making arbitrary sentence")
                next_sentence = text_model.make_sentence()
            if len(wrestlers) > 2 and random.random() > 0.5:
                # This is (at least) the third line in the promo, so we may
                # want to reuse a previous person who spoke
                next_wrestler = wrestlers[-2]
            else:
                next_wrestler = random.choice(wrestler_names)
            next_line = "{}: {}\n".format(next_wrestler.upper(), next_sentence)
            LOG.info("Next line: %s", next_line)
            if any(
                [
                    blacklisted.lower() in next_line.lower()
                    for blacklisted in WRESTLER_BLACKLIST
                ]
            ):
                LOG.info("Line rejected by blacklist")
                continue
            wrestlers.append(next_wrestler)
            if len(promo + next_line) > 280:
                if not promo:
                    # The first line was too long, try generating it again
                    LOG.info(
                        "First line too long (%d); trying again",
                        len(next_line),
                    )
                    continue
                LOG.info("Next line too long; promo complete")
                break
            promo += next_line

        self.post(promo)


if __name__ == "__main__":
    WrestlingPromosBot("WrestlingPromos").run()
